pipeline {
	agent any

	options {
	    skipStagesAfterUnstable()
	}

	stages {
		stage ('Build (Docker)') {

			environment {
				DOCKER_IMAGE = "wtsparadigm/docker-host-proxy"
				DOCKER_IMAGE_TAG = "${TAG_NAME == null ? BRANCH_NAME.replaceAll('master', 'latest') : TAG_NAME}"
				DOCKER_FQ_IMAGE = "${DOCKER_IMAGE}:${DOCKER_IMAGE_TAG}"
			}

			stages {
				stage ('Build image') {
					steps {
						sh 'docker build --force-rm -t ${DOCKER_FQ_IMAGE} .'
					}
				}
				stage ('Publish') {
					when { anyOf { branch 'master'; tag 'v*' } }
					steps {
						withDockerRegistry([credentialsId: 'dockerhub-paradigm-creds', url: '']) {
							sh 'docker push ${DOCKER_FQ_IMAGE}'
						}
					}
				}
			}
			post {
			    always {
					sh 'docker rmi ${DOCKER_FQ_IMAGE}'
				}
			}
		}
	}
	post {
		failure {
			sendEmail()
		}
		unstable {
			sendEmail()
		}
	}
}

void sendEmail() {

	echo "Fetching last commiter informations for head commit"

	withEnv(['GIT_ORIGIN_BRANCH=' + GIT_BRANCH.replaceAll("(origin/)?(.+)", "origin/\$2")]) {
		withEnv(['LAST_BRANCH_COMMIT=' + sh (script: 'git rev-list --max-count=1 \${GIT_ORIGIN_BRANCH}', returnStdout: true).trim()]) {
			lastCommiterEmail = sh (script: 'git log -n1 \${LAST_BRANCH_COMMIT}~1..\${LAST_BRANCH_COMMIT} --no-merges --format=%ce', returnStdout: true).trim()
			lastCommiterName = sh (script: 'git log -n1 \${LAST_BRANCH_COMMIT}~1..\${LAST_BRANCH_COMMIT} --no-merges --format=%cn', returnStdout: true).trim()

			lastCommiter = "\"$lastCommiterName\" <$lastCommiterEmail>"
			echo "Last commiter is '$lastCommiter'"
			recipients = [lastCommiterEmail]
		}
	}

	echo "Fetching last merger informations..."
	lastMergerEmail = sh (script: 'git log -n1 HEAD~1..HEAD --merges --format=%ce', returnStdout: true).trim()
	if (lastMergerEmail && lastMergerEmail != "nobody@nowhere") {
		lastMergerName = sh (script: 'git log -n1 HEAD~1..HEAD --merges --format=%cn', returnStdout: true).trim()
		lastMerger = "\"$lastMergerName\" <$lastMergerEmail>"
		echo "Last merger is '$lastMerger'"
		recipients.add(lastMergerEmail)
	}
	else {
		echo "Last commit wasn't merged; skipping last merger"
	}

	emailext subject: "${currentBuild.currentResult}".toLowerCase().capitalize() + " pipeline build: ${currentBuild.fullDisplayName}",
	body: "Something went wrong during build; please look here for more details: ${env.RUN_DISPLAY_URL}",
	to: recipients.join(","),
	recipientProviders: [[$class: 'RequesterRecipientProvider']]
}
