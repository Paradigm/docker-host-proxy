# docker-host-proxy

Provides a way to forward traffic from an input port on this container to an output port on the host. This can be useful when developing a Swarm-based project, in order to communicate with a service worked on locally outside the regular stack.

# Usage

To use the image, two environment variables must be defined:

* CONTAINER\_INPUT\_PORT: Input port on the container that will listen for traffic to forward. (Default: 8080)
* HOST\_OUTPUT\_PORT: Output port on the host to which traffic will be forwarded. (Default: 8080)

# Example

Here is an example of how to use docker-host-proxy to redirect requests on port 8500 of the container to port 5000 on the host.

	host-proxy:
		image: wtsparadigm/docker-host-proxy:latest
		environment:
		  - CONTAINER_INPUT_PORT=8500
		  - HOST_OUTPUT_PORT=5000