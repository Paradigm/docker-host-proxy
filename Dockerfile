FROM alpine/socat

ENV CONTAINER_INPUT_PORT=8080
ENV HOST_OUTPUT_PORT=8080

COPY docker-entrypoint.sh docker-entrypoint.sh

ENTRYPOINT ./docker-entrypoint.sh